package studentChat;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class ChatGui extends JFrame {

	private static final long serialVersionUID = 1L;
	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatGui frame = new ChatGui();
					frame.setVisible(true);

				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 * @throws FileNotFoundException 
	 */
	public ChatGui() throws FileNotFoundException {
		setResizable(false);
		setTitle("Student Chat");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 320, 350);
		
		JMenuBar menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		JMenu mnFile = new JMenu("File");
		menuBar.add(mnFile);
		
		JMenuItem mntmNew = new JMenuItem("Clear");
		mnFile.add(mntmNew);
		
		JMenuItem mntmExit = new JMenuItem("Exit");
		mntmExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				System.exit(1);
			}
		});
		mnFile.add(mntmExit);
		
		JMenu mnHelp = new JMenu("Help");
		menuBar.add(mnHelp);
		
		JMenuItem mntmSeeGooglecom = new JMenuItem("See google.com");
		mnHelp.add(mntmSeeGooglecom);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setToolTipText("");
		scrollPane.setBounds(6, 6, 308, 197);
		contentPane.add(scrollPane);
		
		JTextArea txtChat = new JTextArea();
		txtChat.setLineWrap(true);
		txtChat.setEditable(false);
		scrollPane.setViewportView(txtChat);
		
		int limit = 20;
		
		String[][] studentNames = {{"Hayden", "Beadles"},
		{"Gerald", "Carson"},
		{"Chad", "Curvin"},
		{"Jayci", "Giles"},
		{"Ian", "Hale"},
		{"Joseph", "Hubbard"},
		{"Jose", "Jara"},
		{"Jonathan", "Langford"},
		{"Trevor", "Marsh"},
		{"Casey", "Mau"},
		{"Bryant", "Morrill"},
		{"Mark", "Richardson"},
		{"Nash", "Stewart"},
		{"Michael", "Zeller"}};
		
		List<Student> students = new ArrayList<Student>();
		BufferedReader file = new BufferedReader(new FileReader("quotes.txt"));
		for (int i = 0; i < studentNames.length; i++){
			Student student = new Student(studentNames[i][0],studentNames[i][1]);
			int x = 0;
			try {
				while (student.addResponse(file.readLine()) < 10){
					if (x >= limit){
						break;
					}
					x++;
				}
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			students.add(student);
		}
		try {
			file.close();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		List<Group> groups = new ArrayList<Group>();
		
		for(Iterator<Student> i = students.iterator(); i.hasNext();){
			Student student1 = i.next();
			Student student2 = i.next(); // This will error if there are an odd number of students.
			Group group = new Group(student1, student2);
			groups.add(group);
		}
		
		Student studentOne = groups.get(1).getStudentOne();
		Student studentTwo = groups.get(1).getStudentTwo();
		
		String[] studentOneReponses = studentOne.getResponses();
		String[] studentTwoReponses = studentTwo.getResponses();
		
		int length = (studentOneReponses.length>studentTwoReponses.length?studentTwoReponses.length:studentOneReponses.length);

		for (int i = 0; i < length; i++){
			txtChat.append(studentOne.getfName() + " ");
			txtChat.append(studentOne.getlName() + ": ");
			txtChat.append(studentOneReponses[i] + "\n");
			txtChat.append(studentTwo.getfName() + " ");
			txtChat.append(studentTwo.getlName() + ": ");
			txtChat.append(studentTwoReponses[i] + "\n");
		}
		
		JTextArea textInput = new JTextArea();
		textInput.addKeyListener(new KeyAdapter() {
			public void keyPressed(KeyEvent e) {
				if (e.getKeyCode()==KeyEvent.VK_ENTER){
					String txtToEnter = textInput.getText();
					txtChat.append("Me: " + txtToEnter + "\n");
					textInput.setText("");
				}
		       
			}
		});
		textInput.setBounds(6, 207, 308, 53);
		contentPane.add(textInput);
		
		JButton btnSubmit = new JButton("Send");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String txtToEnter = textInput.getText();
				txtChat.append("Me: " + txtToEnter + "\n");
				textInput.setText("");
			}
		});
		btnSubmit.setBounds(6, 272, 117, 29);
		contentPane.add(btnSubmit);
	}
	
}
