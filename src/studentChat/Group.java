package studentChat;

public class Group {
	
	private Student student1;
	private Student student2;
	
	public Group (Student s1, Student s2){
		this.student1 = s1;
		this.student2 = s2;
	}
	
	public Student getStudentOne(){
		return this.student1;
	}
	public Student getStudentTwo(){
		return this.student2;
	}
}
