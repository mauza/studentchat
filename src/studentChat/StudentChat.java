package studentChat;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class StudentChat {
	
	
	public static void main(String[] args) throws IOException {
		
		int limit = 20;
		
		String[][] studentNames = {{"Hayden", "Beadles"},
		{"Gerald", "Carson"},
		{"Chad", "Curvin"},
		{"Jayci", "Giles"},
		{"Ian", "Hale"},
		{"Joseph", "Hubbard"},
		{"Jose", "Jara"},
		{"Jonathan", "Langford"},
		{"Trevor", "Marsh"},
		{"Casey", "Mau"},
		{"Bryant", "Morrill"},
		{"Mark", "Richardson"},
		{"Nash", "Stewart"},
		{"Michael", "Zeller"}};
		
		List<Student> students = new ArrayList<Student>();
		BufferedReader file = new BufferedReader(new FileReader("quotes.txt"));
		for (int i = 0; i < studentNames.length; i++){
			Student student = new Student(studentNames[i][0],studentNames[i][1]);
			int x = 0;
			while (student.addResponse(file.readLine()) < 10){
				if (x >= limit){
					break;
				}
				x++;
			}
			students.add(student);
		}
		file.close();
		List<Group> groups = new ArrayList<Group>();
		
		for(Iterator<Student> i = students.iterator(); i.hasNext();){
			Student student1 = i.next();
			Student student2 = i.next(); // This will error if there are an odd number of students.
			Group group = new Group(student1, student2);
			groups.add(group);
		}
		
		Student studentOne = groups.get(1).getStudentOne();
		Student studentTwo = groups.get(1).getStudentTwo();
		
		String[] studentOneReponses = studentOne.getResponses();
		String[] studentTwoReponses = studentTwo.getResponses();
		
		int length = (studentOneReponses.length>studentTwoReponses.length?studentTwoReponses.length:studentOneReponses.length);
		Scanner s = new Scanner(System.in);
		for (int i = 0; i < length; i++){
			System.out.print(studentOne.getfName() + " ");
			System.out.print(studentOne.getlName() + ": ");
			System.out.println(studentOneReponses[i]);
			s.nextLine();
			System.out.print(studentTwo.getfName() + " ");
			System.out.print(studentTwo.getlName() + ": ");
			System.out.println(studentTwoReponses[i]);
			s.nextLine();
		}
		s.close();
		
	}

}
