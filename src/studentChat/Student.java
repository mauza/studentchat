package studentChat;

import java.util.HashSet;
import java.util.Set;

public class Student {
	
	private String fname;
    private String lname;
    private int score;
    private Set<String> responses;

    public Student(String firstName, String lastName){
    	this.fname = firstName;
    	this.lname = lastName;
    	this.score = 0;
    	this.responses = new HashSet<String>();
    	
    }
    public String getfName()
    {
        return this.fname;
    }
    public String getlName()
    {
        return this.lname;
    }

    public int getScore()
    {
        return this.score;
    }
    public boolean setScore(int score)
    {
        this.score = score;
        return true;
    }
    
    public String[] getResponses(){
    	return this.responses.toArray(new String[this.responses.size()]);
    }
    
    public int addResponse(String response){
    	this.responses.add(response);
    	return this.responses.size();
    }
    
    public boolean removeResponse(String response){
    	int startingSize = this.responses.size();
    	this.responses.remove(response);
    	if (startingSize != this.responses.size()){
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    
}
