package studentChat;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Writer;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class GetQuote {
	
	public static String getQuote() throws IOException{
		String url = "http://api.forismatic.com/api/1.0/?method=getQuote&lang=en&format=text";
		URL obj = new URL(url);
		HttpURLConnection con = (HttpURLConnection) obj.openConnection();
		con.setRequestProperty("User-Agent", "Mozilla/5.0");
		
		BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
		String inputLine;
		StringBuffer response = new StringBuffer();
		
		while ((inputLine = in.readLine()) != null){
			response.append(inputLine);
		}
		in.close();
		return response.toString().replaceAll("\\(.*\\)", "");
	}
	
	public static void main(String[] args) throws IOException {
		Writer output;
		output = new BufferedWriter(new FileWriter("quotes.txt", true));
		Set<String> quotes = new HashSet<String>();
		long startTime = System.currentTimeMillis();
		while (System.currentTimeMillis()-startTime<500000){
			quotes.add(getQuote());
			if (quotes.size()>= 500){
				break;
			}
		}
		for(Iterator<String> i = quotes.iterator(); i.hasNext();){
			String quote = i.next();
			output.append(quote);
			output.append("\n");
		}
		output.close();
	}
}
	

